# Weekly-Planner

Small weekly-planner application to keep yourself organized!

#Expectations
Must have the following installed through npm...(npm install "package name here")

-redux
-react-redux
-bootstrap
-semantic-ui-react

#Required HTML links
Must have this linked in html file.

#<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />
