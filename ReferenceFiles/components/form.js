//React imports
import React, { Component } from 'react';

//Material imports
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Form extends Component {

	render() {

		const { taskName, dueDate, priority } = this.props;

		return(
			<div>
				<form>
					<br />
					<Typography variant="subtitle1">
						Please enter a new Task
					</Typography>
					<TextField 
					 	variant="outlined"
						name="taskName"
						value={taskName}
						placeholder="Task Name"
						onChange={event => this.updateEntry(event)}
					/><br/>
					<TextField 
						variant="outlined"
						name="dueDate"
						value={dueDate}
						placeholder="Due Date"
						onChange={event => this.updateEntry(event)}
					/><br />
					<TextField 
						variant="outlined"
						name="priority"
						value={priority}
						placeholder="Priority"
						onChange={event => this.updateEntry(event)}
					/><br />
					<Button
						size="large"
						variant="contained"
						color="secondary"
						onClick={event => this.onSubmit(event)}
					>
						Submit
					</Button>
				</form>
			</div>
		);
	}

	updateEntry = event => {

		const { name, value } = event.target;
		const { updateEntry } = this.props;

		console.log(`Calling updatEntry in form, from: ${name} displaying ${value}`);

		updateEntry(name, value);
	};

	onSubmit = event => {

		const { onSubmit } = this.props;

		event.preventDefault();
		console.log("Form submitted!");

		onSubmit();
	};
};

export default Form;