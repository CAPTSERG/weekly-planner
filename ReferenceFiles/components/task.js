//React imports
import React, { Component } from 'react';

//Component imports
import Form from './form'

/* Task component description:
	Task component will take in values submitted through form component 
	and will pass those upwards to app.
*/


class Task extends Component {

	state = {
		taskName: '',
		dueDate: '',
		priority: ''
	}

	render() {

		const { taskName, dueDate, priority } = this.state;

		return(
			<div>
				<Form 
					taskName={taskName} 
					dueDate={dueDate} 
					priority={priority} 
					updateEntry={this.updateEntry}
					onSubmit={this.onSubmit}
				/>
				<p>
					<br />
				</p>
			</div>
		);
	};

	updateEntry = (name, value) => {
		this.setState({
			[name]: value
		});
	};

	onSubmit = () => {

		const { onSubmit } = this.props;
		onSubmit(this.state);
		this.setState({
			taskName: '',
			dueDate: '',
			priority: ''
		});

	};

};

export default Task;