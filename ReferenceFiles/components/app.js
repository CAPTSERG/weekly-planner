//React imports
import React, { Component } from 'react';

//Redux imports
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addTask } from '../actions/app-actions';

//Component imports
import Task from './task'

//Material imports
import 'typeface-roboto';
import { makeStyles } from '@material-ui/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

//FIX_ME: use this to display the tasks
//{JSON.stringify(this.props.taskList[0],null,2)}

class App extends Component {

	render() {

		const { taskList } = this.props;

		return(
			<div >
				<Typography variant="h1">
					Add A Task Form: v.9000
				</Typography> 
				<Task onSubmit={task => this.onSubmit(task)}/>
				<Typography variant="h2" component="h2">
					Your Task List:
				</Typography>
				<div>
					{taskList.map(task => 
						<Card key={taskList.indexOf(task)}>
							<CardContent>
								<React.Fragment>
									<Typography>
										Task # {taskList.indexOf(task) + 1}
									</Typography>
									<Typography variant="h5" component="h2">
										{task.taskName}
									</Typography>
									<Typography variant="subtitle1">
										Due Date: {task.dueDate}
										<br />
										Priority: {task.priority}
									</Typography>
								</React.Fragment>
							</CardContent>
						</Card>
					)}
				</div>
			</div>

		);
	};

	onSubmit = (task) => {
		console.log("Called onSubmit from App");
		console.log("Task about to be passed to store is: ", task);

		const { addTask } = this.props;

		addTask(task);
	};

};

const mapStateToProps = (state, props) => {
	console.log("Succesfully mapped state to props of app Component");
	return {
		taskList: state.taskList
	};
};

const mapActionsToProps = (dispatch, props) => {
	return bindActionCreators({
			addTask: addTask
		},
		dispatch
	);
}

export default connect(mapStateToProps, mapActionsToProps) (App);