import { ADD_TASK } from "../actions/app-actions";

export default function taskReducer(state = [], { type, payload }) {
	console.log("State inside reducer: ", state);
  switch (type) {
    case ADD_TASK:
      console.log("Adding task to store: ", payload.newTask);
      //state is already an array of task
      return [...state, payload.newTask];
    default:
      return state;
  }
}
