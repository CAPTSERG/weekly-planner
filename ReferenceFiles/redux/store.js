//Redux imports
import { combineReducers, createStore } from "redux";

//Reducer imports
import taskReducer from "../reducers/app-reducer";

//Reducer set up
const rootReducer = combineReducers({
  taskList: taskReducer
});

//Store set up
export const store = createStore(
  rootReducer,
  {
    taskList: [
      {
        taskName: "Feed Dog",
        dueDate: "5/15",
        priority: "High"
      }
    ]
  },
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
