//React imports
import React, { Component } from 'react';

//Redux imports
import { connect } from 'react-redux';

//Semantic-UI imports
import { Header, Divider, Grid, Icon, Label } from 'semantic-ui-react';

//Component imports
import Task from './task/task';
import Event from './event/event';
import EventAdder from './event/eventAdder';
import TaskAdder from './task/taskAdder';

	


class Day extends Component {

	render(){

		const { taskList, eventList, dayOfTheWeek, date } = this.props;

		return(

			<React.Fragment>
				<Grid.Column>
					<Label as="a" color="orange" inverted ribbon >
						{date}
					</Label>
					<Header as="h1">
						{dayOfTheWeek}	
					</Header>
					<Divider horizontal>
						<Icon name="calendar alternate outline" />
						Events
						<Icon name="calendar alternate outline" />
					</Divider>
					{eventList.map(event => 
							<Event 
								key={event.eventName}
								eventName={event.eventName}
								eventDescription={event.eventDescription}
								date={event.date}
								time={event.time}
								eventType={event.eventType}
							/>
					)}
					<br /><br /> 
					<EventAdder />
					<br /><br />
					<Divider horizontal>
						<Icon name="clipboard outline" />
						Tasks
						<Icon name="clipboard outline" />										
					</Divider>
					{taskList.map(task => 
							<Task 
								key={task.taskName}
								taskName={task.taskName}
								taskDescription={task.taskDescription}
								dueDate={task.dueDate}
								priority={task.priority}
							/>
					)}
					<br /><br /> 
					<TaskAdder />
					<br /><br />
				</Grid.Column>
			</React.Fragment>
		);
	};

};

const mapStateToProps = (state) => {
  	console.log("Sucesfully mapped taskList and eventList to props of Day Component: ", state.taskList, state.eventList);
    return {
      taskList: state.taskList,
      eventList: state.eventList
    };
};

export default connect(mapStateToProps) (Day);