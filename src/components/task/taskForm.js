//React imports
import React, { Component } from 'react';

//Component imports
import { taskOptions } from './taskOptions'

//Semantic-UI imports
import { Form, Icon, Header, Segment, Dropdown, Divider, Button } from 'semantic-ui-react';

class TaskForm extends Component {

	state = {
		taskName: "",
		taskDescription: "",
		dueDate: "",
		priority: ""		
	};


	render() {

		const { taskName, taskDescription, priority, dueDate } = this.state;
		const { priorityOptions, dateOptions } = taskOptions;
		const { onSubmit } = this;

		return(
			<React.Fragment>
				<Header as="h2">
					<Icon name="calendar outline alternate" />
					<Header.Content>Add a Task</Header.Content>
				</Header>
				<Divider />
				<Segment raised>
					<Form size="tiny" onSubmit={onSubmit}>

						<Form.Field>
							<label>Task Name</label>
							<input name="taskName" placeholder="Task Name" value={taskName} onChange={this.handleInputChange} />

							<label>Task Description</label>
							<input name="taskDescription" placeholder="Task Description" value={taskDescription} style={{minHeight: 100}} onChange={this.handleInputChange} />
						</Form.Field>

						<Form.Group inline>
							<label>Priority</label>
							<Dropdown selection name="priority" options={priorityOptions} placeholder="priority" value={priority} onChange={this.handleDropdownChange} /><br />
						</Form.Group>

						<Form.Group inline>
							<label>Due Date</label>
							<Dropdown selection name="dueDate" options={dateOptions} placeholder="date" value={dueDate} onChange={this.handleDropdownChange} /><br />
						</Form.Group>

						<Button color="green" size="mini" floated="right" content="Create Event" />

					</Form>
				</Segment>
			</React.Fragment>

		);
	};

	handleDropdownChange = (event, { name, value }) => {
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	handleInputChange = event => {
		const { name, value } = event.target;
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	onSubmit = () => {

		console.log("form props", this.props);
		const { onSubmit } = this.props;

		console.log("Turning in Event Form", this.state);

		onSubmit(this.state);
		this.setState({
			taskName: "",
			taskDescription: "",
			dueDate: "",
			priority: ""	
		});
	};

};

export default TaskForm;