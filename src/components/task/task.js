//React imports
import React, { Component } from 'react';

//Semantic-UI imports
import { Button, Icon, Popup, Header, Divider, Segment } from 'semantic-ui-react';

class Task extends Component {

	constructor(props){
		super(props);

		let color = this.setColor();
		this.state = {
			color: color
		};

	}

	render(){

		const { taskName, dueDate, taskDescription } = this.props;
		const { color } = this.state

		return(
			<React.Fragment>
				<br />
				<Popup trigger={
					<Button icon color={color} labelPosition="left">
						<Icon name="clipboard outline" />
						{taskName}
					</Button>
					}
					on="click"
					position="right center"
				>
					<Header size='medium'>{taskName}: </Header>
					<Divider />
					<Segment raised>
						{taskDescription}
						<br />
						Due date: {dueDate}
					</Segment>
				</Popup>
			</React.Fragment>

		);
	};

	setColor () {
		const { priority } = this.props;

		switch (priority) {

			case "High":
				return "red";

			case "Medium":
				return "orange";

			case "Low":
				return "yellow";

			default:
				return "grey";
		}
	}

};

export default Task;