//React imports
import React, { Component } from 'react';

//Redux imports
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addTask } from '../../actions/taskAdder-actions';

//Component imports
import TaskForm from './taskForm';

//Semantic-UI imports
import { Button, Popup, Icon } from 'semantic-ui-react';

class TaskAdder extends Component {

	render() {
		return(
			<React.Fragment>
				<Popup trigger={
					<Button icon size="mini" color="grey" floated="right" labelPosition="left">
						<Icon name="plus" />
						Add
					</Button>
					}
					on="click"
					position="right center"
				>
					<TaskForm onSubmit={task => this.onSubmit(task)}/>	
				</Popup>
			</React.Fragment>
		);
	};

	onSubmit = task => {
		const { addTask } = this.props;
		console.log("taskAdderProps: ", this.props);
		console.log('About to call the action creator to create: ', task);
		addTask(task);		
	};
};

const mapActionsToProps = ( dispatch, props ) => {
 	return bindActionCreators({
 			addTask: addTask
 		}, 
 		dispatch
 	);
}

export default connect(null, mapActionsToProps) (TaskAdder);