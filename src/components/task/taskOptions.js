export const taskOptions = {
	priorityOptions: [
		{text: "High", value: "High"},
		{text: "Medium", value: "Medium"},
		{text: "Low", value: "Low"},
	],
	dateOptions: [
		{text: "5/16", value: "5/16"},
		{text: "5/17", value: "5/17"},
		{text: "5/18", value: "5/18"},
	],
}