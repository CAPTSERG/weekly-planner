//React imports
import React, { Component } from 'react';

//Redux imports
import { connect } from 'react-redux';

//Semantic-UI imports
import { Grid, Button } from 'semantic-ui-react';

//Component imports
import Day from './day';

class Week extends Component {

	render(){

		const { daysList } = this.props;

		return(
			<React.Fragment>
				<Grid textAlign="center">
					<Grid celled style={{maxWidth: 1800}} columns="equal">
						{daysList.map(day => 
								<Day 
									key={day.date}
									dayOfTheWeek={day.dayOfTheWeek}
									date={day.date}
								/>
						)}
					</Grid>
				</Grid>
				<br />
				<Button floated="right" negative content="Reset Week" onClick={this.handleReset}/>
			</React.Fragment>
		);
	};

	handleReset = () => {
		console.log("handleReset called!");
	};

};

const mapStateToProps = (state) => {
  	console.log("Sucesfully mapped dayList to props of Week Component: ", state);
    return {
      daysList: state.daysList
    };
};

export default connect(mapStateToProps) (Week);