//React imports
import React, { Component } from 'react';

//Redux imports
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addEvent } from '../../actions/eventAdder-actions';

//Component imports
import EventForm from './eventForm';

//Semantic-UI imports
import { Button, Popup, Icon } from 'semantic-ui-react';

class EventAdder extends Component {

	render() {
		return(
			<React.Fragment>
				<Popup trigger={
					<Button icon size="mini" color="grey" floated="right" labelPosition="left">
						<Icon name="plus" />
						Add
					</Button>
					}
					on="click"
					position="right center"
				>
					<EventForm onSubmit={event => this.onSubmit(event)}/>	
				</Popup>
			</React.Fragment>
		);
	};

	onSubmit = event => {
		const { addEvent } = this.props;
		console.log("eventAdderProps: ", this.props);
		console.log('About to call the action creator to create: ', event);
		addEvent(event);
	};
};

const mapActionsToProps = ( dispatch, props ) => {
 	return bindActionCreators({
 			addEvent: addEvent
 		}, 
 		dispatch
 	);
}

export default connect(null, mapActionsToProps) (EventAdder);