//React imports
import React, { Component } from 'react';

//Semantic-UI imports
import { Button, Icon, Popup, Header, Divider, Segment } from 'semantic-ui-react';

class Event extends Component {

	constructor(props){
		super(props);

		let color = this.setColor();
		this.state = {
			color: color
		};

	}

	render(){

		const { eventName, eventDescription, date, time } = this.props;
		const { color } = this.state

		return(
			<React.Fragment>
				<br />
				<Popup trigger={
					<Button icon color={color} labelPosition="left">
						<Icon name="calendar outline alternate" />
						{eventName} {time}
					</Button>
					}
					on="click"
					position="right center"
				>
					<Header size='medium'>{eventName}: </Header>
					<Divider />
					<Segment raised>
						{eventDescription}
						<br />
						Date: {date} Time: {time}
					</Segment>
				</Popup>
			</React.Fragment>

		);
	};

	setColor () {
		const { eventType } = this.props;

		switch (eventType) {

			case "Meeting":
				return "blue";

			case "Doctors":
				return "violet";

			case "Exam":
				return "teal";

			case "Special":
				return "olive";
				
			default:
				return "grey";
		}
	}

};

export default Event;