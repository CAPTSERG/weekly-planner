//React imports
import React, { Component } from 'react';

//Component imports
import { eventOptions } from './eventOptions'

//Semantic-UI imports
import { Form, Icon, Header, Segment, Dropdown, Divider, Button } from 'semantic-ui-react';

class EventForm extends Component {

	state = {
		eventType: "",
		eventName: "",
		eventDescription: "",
		date: "",
		time: "",
	};


	render() {

		const { eventType, eventName, eventDescription, date, time } = this.state;
		const { eventTypeOptions, dateOptions, timeOptions } = eventOptions;
		const { onSubmit } = this;

		return(
			<React.Fragment>
				<Header as="h2">
					<Icon name="calendar outline alternate" />
					<Header.Content>Add an Event</Header.Content>
				</Header>
				<Divider />
				<Segment raised>
					<Form size="tiny" onSubmit={event => onSubmit(event)}>

						<Form.Field>
							<label>Event Type</label>
							<Dropdown selection name="eventType" options={eventTypeOptions} placeholder="Event Type" value={eventType} onChange={this.handleDropdownChange}  />

							<label>Event Name</label>
							<input name="eventName" placeholder="Event Name" value={eventName} onChange={this.handleInputChange} />

							<label>Event Description</label>
							<input name="eventDescription" placeholder="Event Description" value={eventDescription} style={{minHeight: 100}} onChange={this.handleInputChange} />
						</Form.Field>

						<Form.Group inline>
							<label>Date</label>
							<Dropdown selection name="date" options={dateOptions} placeholder="date" value={date} onChange={this.handleDropdownChange} /><br />

							<label>Time</label>
							<Dropdown selection name="time" options={timeOptions} placeholder="date" value={time} onChange={this.handleDropdownChange} /><br />
						</Form.Group>

						<Button color="green" size="mini" floated="right" content="Create Event" />

					</Form>
				</Segment>
			</React.Fragment>

		);
	};

	handleDropdownChange = (event, { name, value }) => {
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	handleInputChange = event => {
		const { name, value } = event.target;
		this.setState({
			...this.state,
			[name]: value
		});
		console.log(`Changed ${name} to: ${value}`);
	};

	onSubmit = () => {

		console.log("form props", this.props);
		const { onSubmit } = this.props;

		console.log("Turning in Event Form", this.state);

		onSubmit(this.state);
		this.setState({
			eventType: "",
			eventName: "",
			eventDescription: "",
			date: "",
			time: "",
		});
	};

};

export default EventForm;