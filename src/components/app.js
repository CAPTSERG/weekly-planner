//React imports
import React, { Component } from 'react';

//Redux imports

//Semantic-UI imports
import { Grid, Header, Container } from 'semantic-ui-react';

//Component imports
import Week from './week';

class App extends Component {

	render() {

		return (
			<div>
					
				<Grid>
					<Grid.Row color="black">					
						<Container text textAlign="center" fluid>
							<br />
								<Header as="h1" inverted color="brown">
									Weekly Planner
								</Header>	
							<br />
						</Container>	
					</Grid.Row>
				</Grid>
				<Week />
			</div>

		);
	};

};

export default App;