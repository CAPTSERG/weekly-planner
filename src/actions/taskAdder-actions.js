export const ADD_TASK = "taskList:addTask";

export function addTask(newTask) {
	console.log("Add-Task action called with:", newTask);

	return {
		type: ADD_TASK,
		payload: {
			newTask
		}
	};
}