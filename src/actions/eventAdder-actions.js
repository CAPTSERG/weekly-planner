export const ADD_EVENT = "eventList:addEvent";

export function addEvent(newEvent) {
	console.log("Add-Event action called with:", newEvent);

	return {
		type: ADD_EVENT,
		payload: {
			newEvent
		}
	};
}