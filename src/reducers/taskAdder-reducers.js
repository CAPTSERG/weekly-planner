import { ADD_TASK } from "../actions/taskAdder-actions";

export default function taskAdderReducer(state =[], { type, payload}) {
	console.log("State inside of reducer: ", state);
	switch (type) {
		case ADD_TASK:
			console.log("Adding new task to store: ", payload.newTask);
			return [...state, payload.newTask];
		default:
			return state;
	}
};