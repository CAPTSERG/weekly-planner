import { ADD_EVENT } from "../actions/eventAdder-actions";

export default function eventAdderReducer(state =[], { type, payload}) {
	console.log("State inside of reducer: ", state);
	switch (type) {
		case ADD_EVENT:
			console.log("Adding new event to store: ", payload.newEvent);
			return [...state, payload.newEvent];
		default:
			return state;
	}
};