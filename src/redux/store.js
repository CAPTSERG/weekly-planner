//Redux imports
import { createStore, combineReducers } from 'redux';

//Reducer imports
import eventAdderReducer from "../reducers/eventAdder-reducers";
import taskAdderReducer from "../reducers/taskAdder-reducers";
import { loadState, saveState } from "../web storage/localStorage";

//Root Reducer setup
const rootReducer = combineReducers({
	eventList: eventAdderReducer,
	taskList: taskAdderReducer
});

//Local Storage and Loading
let persistedState = loadState();

//Setting up deafult for no saved localStorage state
if(persistedState === undefined) {
	persistedState = {
		daysList: [
			{dayOfTheWeek: "Monday", date: "5/16"},
			{dayOfTheWeek: "Tuesday", date: "5/17"},
			{dayOfTheWeek: "Wednsday", date: "5/18"},
			{dayOfTheWeek: "Thrusday", date: "5/19"},
			{dayOfTheWeek: "Friday", date: "5/20"},
			{dayOfTheWeek: "Saturday", date: "5/21"},
			{dayOfTheWeek: "Sunday", date: "5/22"}
		],
		eventList: [],
		taskList: [],
	}
};

//Store setup
export const store = createStore(
	rootReducer,
	persistedState,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()	
);

//update local storage
store.subscribe(() => {
	saveState(store.getState());
});
	
